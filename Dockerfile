FROM alpine:latest as certs
RUN apk --update add ca-certificates

FROM scratch

# update local certs
COPY --from=certs /etc/ssl/ /etc/ssl

COPY bin/proxy /proxy

# gRPC
EXPOSE 5020

# # gRPC-Web
# EXPOSE 5011

# # Web Dev UI
# EXPOSE 5012

ENTRYPOINT ["/proxy"]