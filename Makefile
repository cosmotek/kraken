include version

build:
	-rm -r bin
	mkdir -p bin

	GOOS=linux CGO_ENABLED=0 go build -o bin/proxy main.go

run:
	GRPC_HOST_ADDR=:5020 \
	GRPC_WEB_HOST_ADDR=:5021 \
	CONSUL_ADDR=64.227.10.92:8500 \
	SERVICE_LOG_LEVEL=debug \
	ENABLE_PRETTY_LOG=true \
	SYNC_INTERVAL=5s \
	GOPRIVATE=gitlab.com/ivueit go run main.go

run-secure:
	GRPC_HOST_ADDR=:5020 \
	GRPC_WEB_HOST_ADDR=:5021 \
	CONSUL_ADDR=64.227.10.92:8500 \
	SERVICE_LOG_LEVEL=debug \
	SERVICE_TLS_ENABLED=true \
	SERVICE_TLS_KEY=deploy/local/certs/server.key \
	SERVICE_TLS_CERT=deploy/local/certs/server.crt \
	ENABLE_PRETTY_LOG=true \
	SYNC_INTERVAL=5s \
	GOPRIVATE=gitlab.com/ivueit go run main.go

localdev-certs:
	cd deploy/local/certs; openssl genrsa -out server.key 2048
	cd deploy/local/certs; openssl req -new -x509 -sha256 -key server.key -out server.crt -days 3650
	cd deploy/local/certs; openssl req -new -sha256 -key server.key -out server.csr
	cd deploy/local/certs; openssl x509 -req -sha256 -in server.csr -signkey server.key -out server.crt -days 3650

.PHONY: run build run-secure localdev-certs

docker: build
	docker build -t ivueitv3/ingress_controller:${VERSION} .
	@echo "to push image execute:\ndocker push ivueitv3/ingress_controller:${VERSION}\n"