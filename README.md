# kraken

> Cloud gRPC ingress controller for Hashistack-Clusters (Nomad + Consul)

Kraken uses Consul to search for services with the tag "ingress_linked", pulls in the service
address and meta, and uses that to configure a generalized gRPC proxy for public ingress targeted
at tagged services.

### Valid Meta

+ "ingress_type" marks the traffic type of the service, currently only "grpc" is supported.
+ "ingress_grpc_enable_web_wrapper" enables a built-in gRPC-Web proxy when set to "yes", "1" or "true"
+ "ingress_grpc_whitelist_services" whitelists a set of comma-separated gRPC services (e.g "serviceA, serviceB, serviceC")
+ "ingress_grpc_blacklist_services" blacklists a set of comma-separated gRPC services (e.g "serviceA, serviceB, serviceC")
// TODO add reflection endpoint configuration.

# LocalDev Certs and TLS

To create certs, run `make localdev-certs`, You should get prompted for several fields, ignore all but the FQDN field,
which should be set to "localhost".

Once complete, you should have serveral files in `deploy/local/certs`. Your server will use `server.crt` and `server.key` to
provide a TLS connection. You will need to use the `server.crt` file to connect to the server from any standard GRPC clients
since the certs created for localdev are not signed by a valid public CA.

Executing `make run-secure` will start the proxy application with the generated self-signed certs.