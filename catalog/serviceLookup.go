package catalog

import (
	"errors"
	"fmt"
	"strings"

	"github.com/hashicorp/consul/api"
)

var ErrNoServices = errors.New("no services found")

type Service struct {
	Name   string
	Host   string
	Config map[string]string

	Datacenter     string
	ServiceID      string
	Node           string
	ConsulModIndex uint64
	ConsulID       string
}

type Client struct {
	client *api.Client
}

func NewClient(url string) (*Client, error) {
	conf := api.DefaultConfig()
	conf.Address = url
	conf.Scheme = "http"

	client, err := api.NewClient(conf)
	if err != nil {
		return nil, err
	}

	return &Client{client: client}, nil
}

func (c *Client) FindLinkedServices() ([]Service, error) {
	client := c.client
	res, _, err := client.Catalog().Services(nil)
	if err != nil {
		return nil, err
	}

	services := make([]string, 0)
	for service, tags := range res {
		for _, tag := range tags {
			if tag == "ingress_linked" {
				services = append(services, service)
			}
		}
	}

	if len(services) < 1 {
		return nil, ErrNoServices
	}

	consulServices := make([]Service, 0)
	for _, svc := range services {
		results, _, err := client.Catalog().Service(svc, "", nil)
		if err != nil {
			return nil, err
		}

		for _, result := range results {
			// strip out all meta that doesn't relate to ingress config
			meta := make(map[string]string)
			for key, val := range result.ServiceMeta {
				if strings.HasPrefix(key, "ingress_") {
					meta[key] = val
				}
			}

			consulServices = append(consulServices, Service{
				Name:           result.ServiceName,
				Host:           fmt.Sprintf("%s:%d", result.ServiceAddress, result.ServicePort),
				Config:         meta,
				Node:           result.Node,
				Datacenter:     result.Datacenter,
				ServiceID:      result.ServiceID,
				ConsulID:       result.ID,
				ConsulModIndex: result.ModifyIndex,
			})
		}
	}

	return consulServices, nil
}
