module gitlab.com/cosmotek/kraken

go 1.13

require (
	github.com/desertbit/timer v0.0.0-20180107155436-c41aec40b27f // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/consul/api v1.3.0
	github.com/hashicorp/go-rootcerts v1.0.2 // indirect
	github.com/improbable-eng/grpc-web v0.12.0
	github.com/jhump/protoreflect v1.6.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mwitkow/go-conntrack v0.0.0-20190716064945-2f068394615f // indirect
	github.com/mwitkow/grpc-proxy v0.0.0-20181017164139-0f1106ef9c76 // indirect
	github.com/prometheus/client_golang v1.7.1
	github.com/rs/cors v1.7.0
	github.com/rs/zerolog v1.18.0
	github.com/vgough/grpc-proxy v0.0.0-20191207203309-13d1aa04a5a6
	gitlab.com/ivueit/iv3api v0.8.20
	goji.io v2.0.2+incompatible
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a
	google.golang.org/grpc v1.27.1
)
