package grpc

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/cosmotek/kraken/metrics"

	"github.com/vgough/grpc-proxy/proxy"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type Director struct {
	NamedServiceResolver NamedServiceResolver
	Logger               zerolog.Logger
	Collector            *metrics.Collector
}

func (g *Director) Connect(ctx context.Context, fullMethodName string) (context.Context, *grpc.ClientConn, error) {
	logger := g.Logger

	logger.Trace().Str("method", fullMethodName).Msg("received request")
	segs := strings.Split(fullMethodName, "/")
	if len(segs) < 2 {
		return ctx, nil, status.Errorf(codes.InvalidArgument, "Failed to parse requset: Service name is of invalid format")
	}

	serviceName := segs[1]
	ticker := time.NewTicker(time.Second * 5)

	retryDeadline := time.Now().Add(time.Minute * 3)
retryResolve:

	dst, ok := g.NamedServiceResolver.Resolve(serviceName)
	if !ok {
		if time.Now().After(retryDeadline) {
			return ctx, nil, status.Errorf(codes.Unimplemented, fmt.Sprintf("Failed to proxy call: Unknown GRPC service '%s'", serviceName))
		}

		<-ticker.C
		logger.Debug().Msg("retrying in-flight request which failed to resolve")
		goto retryResolve
	}

	// clean up the ticker
	ticker.Stop()

	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		ctx = metadata.NewOutgoingContext(ctx, md.Copy())
	}

	g.Collector.ConnectionCount.Inc()
	conn, err := grpc.DialContext(ctx, dst, grpc.WithCodec(proxy.Codec()), grpc.WithInsecure())
	if err != nil {
		status, ok := status.FromError(err)
		if ok {
			if status.Code() == codes.Unavailable || status.Code() == codes.Unknown || status.Code() == codes.Unimplemented {
				g.NamedServiceResolver.Remove(serviceName, dst)
				logger.Error().Str("dst", dst).Err(err).Msg("failed to proxy call for unknown reason, removing record from cache")
			}
		}
	}

	return ctx, conn, err
}

func (d *Director) Release(ctx context.Context, conn *grpc.ClientConn) {
	d.Logger.Debug().Msg("releasing client connection")
	d.Collector.ConnectionCount.Dec()
	conn.Close()
}
