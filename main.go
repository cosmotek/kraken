package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cosmotek/kraken/grpc"
	"gitlab.com/cosmotek/kraken/metrics"
	"gitlab.com/ivueit/iv3api/service/log"
)

// env {
// 	"CONSUL_HOST" = "consul.service.dc1.consul:8500"
// 	"CONSUL_SYNC_INTERVAL" = "10s"
// 	"INGRESS_LOADBALANCING_ENABLED" = "true" (this means to round-robin proxy to addresses)
// 	"INGRESS_AUTOREDIAL_ENABLED" = "true" (this means to try other instances if a proxy fails, failure will also trigger a sync with consul to update the service list)
// TODO add GRPC WEB support config
// TODO add grpc config options
// TODO add ui config options
// }

// ports 5000 (grpc) and 5001(grpcweb)

// name for logging and errors
// type of service which dictates source port
// destination port

// TODO ensure graceful exit support in case there is in-flight data and requests
// that need to be completed before exit. This mode should prevent new sessions
// from being created, but allow completion of existing before exit.

type config struct {
	GRPCWebHostAddr string `envconfig:"GRPC_WEB_HOST_ADDR" required:"false"`
	GRPCHostAddr    string `envconfig:"GRPC_HOST_ADDR" required:"true"`

	ConsulAddr   string        `envconfig:"CONSUL_ADDR" required:"true"`
	SyncInterval time.Duration `envconfig:"SYNC_INTERVAL" default:"3m"`

	ProfilerEnabled bool   `split_words:"true" default:"false"`
	ProfilerPort    string `split_words:"true" default:"5003"`

	ServiceTLSEnabled bool   `envconfig:"SERVICE_TLS_ENABLED" required:"false" default:"false"`
	ServiceTLSKey     string `envconfig:"SERVICE_TLS_KEY" required:"false"`
	ServiceTLSCert    string `envconfig:"SERVICE_TLS_CERT" required:"false"`
	ServiceLogLevel   string `required:"true" split_words:"true"`
	EnablePrettyLog   bool   `required:"false" split_words:"true" default:"false"`

	MetricsPort string `split_words:"true" default:"5003"`
	Env         string `required:"true"`
}

func main() {
	conf := config{}
	err := envconfig.Process("", &conf)
	if err != nil {
		panic(err)
	}

	level, err := log.ParseLogLevel(conf.ServiceLogLevel)
	if err != nil {
		panic(err)
	}

	logger := log.New(conf.EnablePrettyLog, "ingress_ctrl", level)
	logger.Info().
		Str("addr", conf.GRPCHostAddr).
		Str("sync_interval", conf.SyncInterval.String()).
		Msg("starting proxy server")

	// set up metrics endpoint
	collector, err := metrics.New(metrics.Config{Env: conf.Env, Port: conf.MetricsPort})
	if err != nil {
		logger.Fatal().Err(err).Msg("server error: failed to init metrics collector")
	}

	ctx, cancel := context.WithCancel(context.Background())
	nsr := grpc.NewConsulNSR(ctx, conf.ConsulAddr, conf.SyncInterval, logger)

	proxy := grpc.GRPCProxy{
		Collector:            collector,
		Logger:               logger,
		NamedServiceResolver: nsr,
		TLSEnabled:           conf.ServiceTLSEnabled,
		TLSKeyFile:           conf.ServiceTLSKey,
		TLSCertFile:          conf.ServiceTLSCert,
	}

	if conf.GRPCWebHostAddr != "" {
		go func() {
			err := proxy.ServeGRPCWeb(conf.GRPCWebHostAddr, []string{"*"}, level == log.LevelDebug)
			if err != nil {
				logger.Fatal().Err(err).Msg("grpc web endpoint exited with error")
			}
		}()
	}

	if conf.GRPCHostAddr != "" {
		go func() {
			err := proxy.ServeGRPC(conf.GRPCHostAddr)
			if err != nil {
				logger.Fatal().Err(err).Msg("grpc endpoint exited with error")
			}
		}()
	}

	if conf.ProfilerEnabled {
		// Run `go tool pprof -lines http://localhost:5003/debug/pprof/heap`
		go http.ListenAndServe(fmt.Sprintf(":%s", conf.ProfilerPort), nil)
	}

	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, os.Interrupt)
	go func() {
		// wait for killsig
		<-sigs
		logger.Info().Msg("kill signal received, attempting graceful exit")

		// send cancel sig over context
		cancel()
		done <- true
	}()

	<-done
}
