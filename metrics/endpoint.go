package metrics

import (
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"goji.io"
	"goji.io/pat"
)

type Config struct {
	Env  string
	Port string
}

type Collector struct {
	ConnectionCount prometheus.Gauge
}

func New(conf Config) (*Collector, error) {
	// collects process info
	err := prometheus.Register(prometheus.NewProcessCollector(prometheus.ProcessCollectorOpts{
		Namespace:    conf.Env,
		ReportErrors: false,
	}))
	if err != nil {
		return nil, err
	}

	connGauge := prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: conf.Env,
		Name:      "trafficrouter_conns",
	})

	err = prometheus.Register(connGauge)
	if err != nil {
		return nil, err
	}

	mux := goji.NewMux()
	mux.Handle(pat.New("/metrics"), promhttp.Handler())

	// listen for collection calls
	go http.ListenAndServe(fmt.Sprintf(":%s", conf.Port), mux)

	return &Collector{
		ConnectionCount: connGauge,
	}, nil
}
